# timeli-dataingestion

This project pulls data(INRIX and Wavetronix) in XML format and converts to csv file and stores as realtime and perday data. 

To run this project :

1. git clone https://rbyna@bitbucket.org/rbyna/timeli-dataingestion.git
2. In application.properties, change the root path property "archive.root.folder" to your local location : e.g. /Users/ravitejarajabyna/ra/datasource/datadrive2

3. mvn clean install
4. java -jar target/<jar filename>.jar

On Master Branch:
-----------------
To run InrixJob, enable "inrix.scheduling.enable" to true
To run WavetronixJob, enable "wavetronics.scheduling.enable" to true
Each method in above Jobs fetches the data for various feed and stores as xml or in database as per the need.
Currently, only inrix feed and wavetronics data is fetched so executeArchiveInrixFeed() and executeArchiveTrafficData() respectively are enabled.
Rest Calls are made to fetch each data.
For inrix, data is stored as a csv file and everytime the csv file is replaced with new one every minute. 
For wavetronix, data is stored as a csv file and everytime the csv file is replaced with new one every 20 seconds. 
Also, smoothing is done for wavetronix data (Wavelet Tranform Algo) since the speed data received for wavetronix is for each lane for each detector.

Please check with Vamsi/Raviteja for more details.

Also, there is code for retriving Camera Inventroy and DMS Inventory and store it in CosmosDB. This is not enabled now.

Inrix Output: ${archive.root.folder}/inrix/realtimedata/ and ${archive.root.folder}/inrix/perdaydata/
Wavetronix Output: ${archive.root.folder}/iadot/realtimedata/ and ${archive.root.folder}/iadot/perdaydata/
Wavetronix Smoothing Output: ${archive.root.folder}/iadot/smoothingdata/realtimedata/ and ${archive.root.folder}/iadot/smoothingdata/perdaydata/


Virtual Machine where it is installed:

1. ssh team@23.101.126.159
2. ssh -i "timeli.pem" ubuntu@ec2-3-91-13-27.compute-1.amazonaws.com



