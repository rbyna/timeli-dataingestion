<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java" >
<xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
<!-- Code,SPEED,AVG,Reference,Travel,Time -->
<xsl:for-each select="//Segment">
<xsl:value-of select="concat(@code,',',@c-value,',',@segmentClosed,',',@score,',',@speed,',',@average,',',@reference,',',@travelTimeMinutes,',',java:format(java:java.text.SimpleDateFormat.new('yyyy-MM-dd HH:mm:ss z'), java:java.util.Date.new()),'&#xA;')"/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>