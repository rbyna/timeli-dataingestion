package com.intrans.timeli.dataingestion.scheduler;

import com.intrans.timeli.dataingestion.handlers.AsyncCallBack;
import com.intrans.timeli.dataingestion.handlers.Callback;
import com.intrans.timeli.dataingestion.process.DataArchiveProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@ConditionalOnProperty(value = "inrix.scheduling.enable", havingValue = "true", matchIfMissing = false)
@Component
public class InrixJob {

    @Autowired
    private DataArchiveProcess dataArchiveProcess;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Scheduled(cron = "${job.schedule.inrix.cron}")
    public void executeArchiveInrixFeed() {
        new AsyncCallBack(new Callback() {

            @Override
            public void call() {
                try {
                    dataArchiveProcess.archiveInrixFeed();
                } catch (Exception e) {
                    // TODO add logging
                    e.printStackTrace();
                }

            }
        }).execute(taskExecutor);
    }

    @Scheduled(cron = "${job.schedule.inrix.delete.cron}")
    public void scheduleDeleteWavetrinixYesterdayData() {
        new AsyncCallBack(new Callback() {
            @Override
            public void call() {
                try {
                    System.out.println("Started inrix delete");
                    dataArchiveProcess.deleteData();
                } catch (Exception e) {
                    //TODO: add logging
                    e.printStackTrace();
                }

            }
        }).execute(taskExecutor);
    }

    @Scheduled(cron = "${job.schedule.rawinrix.delete.cron}")
    public void scheduleRawInrixData() {
        new AsyncCallBack(new Callback() {
            @Override
            public void call() {
                try {
//                    System.out.println("Started raw inrix delete");
                    dataArchiveProcess.deleteRawInrixData();
                } catch (Exception e) {
                    //TODO: add logging
                    e.printStackTrace();
                }

            }
        }).execute(taskExecutor);
    }



}
