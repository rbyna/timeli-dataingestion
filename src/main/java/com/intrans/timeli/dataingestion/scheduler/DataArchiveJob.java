package com.intrans.timeli.dataingestion.scheduler;

import com.intrans.timeli.dataingestion.constants.ExternalConfig;
import com.intrans.timeli.dataingestion.handlers.AsyncCallBack;
import com.intrans.timeli.dataingestion.handlers.Callback;
import com.intrans.timeli.dataingestion.process.DataArchiveProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Schedular job for data archiving.
 *
 */

//@Component
public class DataArchiveJob {
	@Autowired
	private DataArchiveProcess dataArchiveProcess;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;



	/**
	 * Execute the archive job on server startup.
	 * 
	 * @throws Exception
	 */
	@PostConstruct
	public void runOnStartup() {
//		System.out.println(externalConfig.getEnableWavetronix());
//		if (externalConfig.getEnableWavetronix()) {
//			//executeArchiveTrafficData();
//		}//Fetch Wavetronics
//		//executeArchiveSensorInventoryData();
//		// executeArchiveTrafficIncidentData();
//		// executeArchiveRealTimeFlowData();
////		if (externalConfig.getEnableWaze()) executeArchiveWazeFeed();
//		System.out.println(externalConfig.getEnableInrix());
//		if (externalConfig.getEnableInrix()) executeArchiveInrixFeed();// Fetch Inrix
//		if (externalConfig.getEnableCamera()) executeArchiveCameraInventory();
//		if (externalConfig.getEnableDMS()) executeArchiveDMSInventory();
//		if (externalConfig.getEnableWorkzone()) executeArchiveWorkzoneInventory();
	}


//	/**
//	 * Job to generate Traffic Date archiving.
//	 *
//	 * @throws Exception
//	 */
//	@Scheduled(cron = "${job.schedule.trafficDetectorData.cron}")
//	public void executeArchiveTrafficData() {
//		new AsyncCallBack(new Callback() {
//
//			@Override
//			public void call() {
//				try {
//					// System.out.println("Starting detector archive job at :" +
//					// DateUtils.getCurrentDate());
//					dataArchiveProcess.archiveTrafficDetectorData(HttpMethod.GET);
//					// System.out.println("Completed detector archive job at :"
//					// + DateUtils.getCurrentDate());
//				} catch (Exception e) {
//					// TODO Add logging
//					e.printStackTrace();
//				}
//
//			}
//		}).execute(taskExecutor);
//
//	}

	@Scheduled(cron = "${job.schedule.inrix.cron}")
	public void executeArchiveInrixFeed() {
		new AsyncCallBack(new Callback() {

			@Override
			public void call() {
				try {
					// System.out.println("Starting Inrix feed archive job at :
					// " + DateUtils.getCurrentDate());
					dataArchiveProcess.archiveInrixFeed();
					// System.out.println("Completed Inrix feed archive job at :
					// " + DateUtils.getCurrentDate());
				} catch (Exception e) {
					// TODO add logging
					e.printStackTrace();
				}

			}
		}).execute(taskExecutor);
	}

	/**
	 * Archive DMS Inventory Information every day.
	 */
	@Scheduled(cron = "${job.schedule.sensorInventory.cron}")
	public void executeArchiveDMSInventory() {
		new AsyncCallBack(new Callback() {

			@Override
			public void call() {
				try {
					dataArchiveProcess.archiveDMSInventory();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}).execute(taskExecutor);
	}
	/**
	 * Job to archive Camera Inventory Information.
	 */
	@Scheduled(cron = "${job.schedule.sensorInventory.cron}")
	public void executeArchiveCameraInventory() {
		new AsyncCallBack(new Callback() {

			@Override
			public void call() {
				try {
					// System.out.println("Starting Camera Inventory archive job
					// at : " + DateUtils.getCurrentDate());
					dataArchiveProcess.archiveCameraInventoryInformation();
					// System.out.println("Completed Camera Inventory archive
					// job at : " + DateUtils.getCurrentDate());
				} catch (Exception e) {
					// TODO add logging
					e.printStackTrace();
				}

			}
		}).execute(taskExecutor);
	}

	private void executeArchiveWorkzoneInventory() {


	}


}
