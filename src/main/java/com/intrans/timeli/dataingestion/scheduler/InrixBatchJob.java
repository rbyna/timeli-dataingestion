package com.intrans.timeli.dataingestion.scheduler;

import com.intrans.timeli.dataingestion.handlers.AsyncCallBack;
import com.intrans.timeli.dataingestion.handlers.Callback;
import com.intrans.timeli.dataingestion.process.DataArchiveProcess;
import com.microsoft.azure.datalake.store.ADLStoreClient;
import com.microsoft.azure.datalake.store.oauth2.AccessTokenProvider;
import com.microsoft.azure.datalake.store.oauth2.ClientCredsTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@ConditionalOnProperty(value = "inrix.batch.scheduling.enable", havingValue = "true", matchIfMissing = false)
@Component
public class InrixBatchJob {

    private static String accountFQDN = "hadoopbatch.azuredatalakestore.net";  // full account FQDN, not just the account name
    private static String clientId = "b7c45e99-47d2-4fc1-a5ef-75bdd5b2f30d";
    private static String authTokenEndpoint = "https://login.microsoftonline.com/c62c238f-6e50-4ad6-905c-4fd61b5e3654/oauth2/token";
    private static String clientKey = "WAfXEgmcSmsTdJgj0zkRdGFBT7uJ/lcIUvpjVKi+XNU=";

    private ADLStoreClient client;

    @PostConstruct
    public void enable() throws IOException {
        initializeAzureDataLakeStore();
    }


    private void initializeAzureDataLakeStore() throws IOException {
        AccessTokenProvider provider = new ClientCredsTokenProvider(authTokenEndpoint, clientId, clientKey);
//            AccessTokenProvider provider = new DeviceCodeTokenProvider("b7c45e99-47d2-4fc1-a5ef-75bdd5b2f30d");
        client = ADLStoreClient.createClient(accountFQDN, provider);

        // create directory
        client.createDirectory("/datadrive2/inrix/");

    }

    @Autowired
    private DataArchiveProcess dataArchiveProcess;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Scheduled(cron = "${job.schedule.inrix.cron}")
    public void executeArchiveInrixFeed() {
        new AsyncCallBack(new Callback() {

            @Override
            public void call() {
                try {
                    dataArchiveProcess.archiveInrixBatchDataInDataLake();
                } catch (Exception e) {
                    // TODO add logging
                    e.printStackTrace();
                }

            }
        }).execute(taskExecutor);
    }
}
