package com.intrans.timeli.dataingestion.framework;


import java.util.Arrays;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.intrans.timeli.dataingestion.entities.RawInrixData;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = "com.intrans.timeli.dataingestion.*")
@EnableMongoRepositories(basePackages = { "com.intrans.timeli.dataingestion.*" })
@EnableScheduling
@EnableCaching
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public JettyEmbeddedServletContainerFactory  jettyEmbeddedServletContainerFactory() {
        JettyEmbeddedServletContainerFactory jettyContainer =
                new JettyEmbeddedServletContainerFactory();

        jettyContainer.setPort(8081);
        jettyContainer.setContextPath("/home");
        return jettyContainer;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(10);
        return taskExecutor;
    }

    /**
     * Customized {@link RestTemplate} with XML Binding options.
     *
     * @return {@link RestTemplate}
     */
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
            if (restTemplate.getMessageConverters().get(i) instanceof MappingJackson2XmlHttpMessageConverter) {
                restTemplate.getMessageConverters().set(i, mappingJackson2XmlHttpMessageConverter());
            }
        }
        return restTemplate;
    }

    /**
     * Modified XML Mapper.
     *
     * @return {@link MappingJackson2XmlHttpMessageConverter}
     */
    @Bean
    public MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter() {
        MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter = new MappingJackson2XmlHttpMessageConverter();
        XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mappingJackson2XmlHttpMessageConverter.setObjectMapper(mapper);
        return mappingJackson2XmlHttpMessageConverter;
    }


    @Bean
    public MongoClient mongo() {
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://timelidb:NXQf7S3iOdWdRS9kswt8YrGOJhjUKVti1lYK9kQ24KSZMIkYI8zYfIeRCyL76icFNuqRr87UTjFhKfQ6aZ61HA==@timelidb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"));

//        return new MongoClient("localhost");
        return mongoClient;
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), "timelidb");
    }
}
