package com.intrans.timeli.dataingestion.convertors;

import com.intrans.timeli.dataingestion.utils.TimeliUtils;
import com.microsoft.azure.datalake.store.ADLFileOutputStream;
import com.microsoft.azure.datalake.store.ADLStoreClient;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

/**
 * Converts XMLfile to CSVfile.
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class XML2CSVConverter {

	/**
	 * Converts xmlSource into csvDest based on the stylesheet transformer. Make
	 * sure that headers are already added to the csvFile before calling this
	 * method.
	 * 
	 * @param xmlSource
	 * @param xslt
	 * @param csvDest
	 * @throws Exception
	 */
	public static void convert(File xmlSource, InputStream xslt, File csvDest) throws Exception {
		convert(new FileInputStream(xmlSource), xslt, csvDest);
	}

	public static void convert(InputStream xmlStream, InputStream xslt, File csvDest) throws Exception {
		assert (xmlStream != null && csvDest != null && xslt != null);
		Document document = TimeliUtils.parseXML(xmlStream);
		StreamSource stylesource = new StreamSource(xslt);
		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
		Source source = new DOMSource(document);
		OutputStream os = new FileOutputStream(csvDest, true);
		Result outputTarget = new StreamResult(os);
		transformer.transform(source, outputTarget);
	}

	public static void convert(InputStream xmlStream, InputStream xslt, File csvDest, String datalakeFilename, ADLStoreClient client) throws Exception {
		assert (xmlStream != null && csvDest != null && xslt != null);
		Document document = TimeliUtils.parseXML(xmlStream);
		StreamSource stylesource = new StreamSource(xslt);
		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
		Source source = new DOMSource(document);
		OutputStream os = new FileOutputStream(csvDest, true);
		Result outputTarget = new StreamResult(os);
		transformer.transform(source, outputTarget);
		FileInputStream fileInputStream = new FileInputStream(csvDest);
		byte[] bytes = IOUtils.toByteArray(fileInputStream);
		ADLFileOutputStream appendStream = client.getAppendStream(datalakeFilename);
		appendStream.write(bytes);
		appendStream.close();
	}
}
