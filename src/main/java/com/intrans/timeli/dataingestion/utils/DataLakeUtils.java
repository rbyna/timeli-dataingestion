package com.intrans.timeli.dataingestion.utils;

import com.microsoft.azure.datalake.store.ADLStoreClient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataLakeUtils {

    /**
     * Returns the today directory path in the given parent folder. If directory
     * doesn't exist then creates one.
     *
     * @param rootFolder
     * @return
     * @throws IOException
     */
    public static Path getTodayDirectory(String rootFolder, ADLStoreClient client) throws IOException {
        Path path = Paths.get(rootFolder, DateUtils.getDefaultCurrentDateStr());
        if (!Files.exists(path)) {
            makeDirectory(path, client);
        }
        return path;
    }

    public static void makeDirectory(Path path, ADLStoreClient client) throws IOException {
        client.createDirectory(path.getFileName().toAbsolutePath().toString());
    }
}
