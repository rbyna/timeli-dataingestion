package com.intrans.timeli.dataingestion.repository;

public interface RawInrixDataCustom {

    Integer queryByTimestamp(long startTime, long endTime);

}
