package com.intrans.timeli.dataingestion.repository;

import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;


@Component
public class RawInrixDataDaoImpl implements RawInrixDataCustom {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Integer queryByTimestamp(long startTime, long endTime) {
        Query query = new Query(Criteria.where("time.value").gte(startTime).lte(endTime));
        WriteResult result = mongoTemplate.remove(query, "rawinrixdata");
        if(result!=null)
            return result.getN();
        else
            return 0;
    }
}
