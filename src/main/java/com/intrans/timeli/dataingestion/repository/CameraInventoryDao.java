package com.intrans.timeli.dataingestion.repository;

import com.intrans.timeli.dataingestion.entities.CameraInventory;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CameraInventoryDao extends MongoRepository<CameraInventory, String> {

	CameraInventory findByDeviceName(String deviceName);
}
