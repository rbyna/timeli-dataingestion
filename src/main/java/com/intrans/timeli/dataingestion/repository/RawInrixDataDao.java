package com.intrans.timeli.dataingestion.repository;

import com.intrans.timeli.dataingestion.entities.RawInrixData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RawInrixDataDao extends MongoRepository<RawInrixData, String>, RawInrixDataCustom {
}
