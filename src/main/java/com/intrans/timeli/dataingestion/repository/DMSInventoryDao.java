package com.intrans.timeli.dataingestion.repository;

import com.intrans.timeli.dataingestion.entities.DMSInventories;
import com.intrans.timeli.dataingestion.entities.DMSInventory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Mongo DAO for accessing the {@link DMSInventories} collection.
 * 
 * @author krishnaj
 *
 */
public interface DMSInventoryDao extends MongoRepository<DMSInventory, String> {

	List<DMSInventory> findByArchiveTimeBetween(long startTime, long endTime);

}
