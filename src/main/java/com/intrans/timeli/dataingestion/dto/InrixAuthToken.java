package com.intrans.timeli.dataingestion.dto;

import org.apache.commons.lang3.time.DateUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

/**
 * Stores Inrix Auth token and Expiry time
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Mar 30, 2017
 *
 */
public class InrixAuthToken implements Serializable {

	private static final long serialVersionUID = 6469559845892213602L;

	private String authToken;
	private String expiryTime;

	public InrixAuthToken() {
	}

	public InrixAuthToken(String authToken, String expiryTime) {
		super();
		this.authToken = authToken;
		this.expiryTime = expiryTime;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getExpiryTimeStr() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	public Date getExpiryTime() throws ParseException {
		return DateUtils.parseDate(getExpiryTimeStr(), "yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

}
