package com.intrans.timeli.dataingestion.constants;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

//@Component
public class ExternalConfig {

    private Properties prop;

    public boolean getEnableInrix() {
        return Boolean.valueOf(prop.get("enableInrix").toString());
    }

    public boolean getEnableWavetronix() {
        return Boolean.valueOf(prop.get("enableWavetronix").toString());
    }

    public boolean getEnableWaze() {
        return Boolean.valueOf(prop.get("enableWaze").toString());
    }

    public boolean getEnableCamera() {
        return Boolean.valueOf(prop.get("enableCamera").toString());
    }

    public boolean getEnableDMS() {
        return Boolean.valueOf(prop.get("enableDMS").toString());
    }

    private String enableInrix;

    private String enableWavetronix;

    private String enableWaze;

    private String enableCamera;

    private String enableDMS;

    public boolean getEnableWorkzone() {
        return Boolean.valueOf(prop.get("enableWorkzone").toString());
    }

    private String enableWorkzone;

    @PostConstruct
    public void initialize() {
        prop = new Properties();
        try {
            File jarPath=new File(ExternalConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            String propertiesPath=jarPath.getParentFile().getAbsolutePath();
            System.out.println(" propertiesPath-"+propertiesPath);
            prop.load(new FileInputStream("./dataingestion.properties"));
            System.out.println(Boolean.valueOf(prop.get("enableInrix").toString()));
            System.out.println(Boolean.valueOf(prop.get("enableWavetronix").toString()));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }





}
