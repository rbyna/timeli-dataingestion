package com.intrans.timeli.dataingestion.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Stores configuration constant files from application.properties file.
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 20, 2017
 *
 */
@Component
public class ConfigConstants {

	@Value("${wavetronix.isCloud}")
	private String cloud;

	// API variables

	// IADOT
	@Value("${iadot.api.trafficData.url}")
	private String trafficDataReqUrl;
	@Value("${iadot.api.sensorInventory.url}")
	private String sensorInventoryReqUrl;
	@Value("${iadot.api.trafficData.get.params}")
	private String msgTrafficDataReqParam;
	@Value("${iadot.api.sensorInventory.get.params}")
	private String msgInventoryReqParam;
	@Value("${iadot.api.cameraInventory.url}")
	private String cameraInventoryUrl;
	@Value("${iadot.api.dmsInventory.url}")
	private String dmsInventoryUrl;
	// HERE
	@Value("${here.api.trafficIncident.url}")
	private String trafficIncidentUrl;
	@Value("${here.api.realTimeFlow.url}")
	private String realTimeFlowUrl;
	@Value("${here.api.iowaDOT.userId}")
	private String iowaDOTUsername;
	@Value("${here.api.iowaDOT.password}")
	private String iowaDOTPassowrd;
	// Waze
	@Value("${waze.api.rss.XML.url}")
	private String wazeFeedXmlurl;
	@Value("${waze.api.rss.JSON.url}")
	private String wazeFeedJsonUrl;
	// Inrix
	@Value("${inrix.api.base.url}")
	private String inrixBaseUrl;
	@Value("${inrix.vendor.id}")
	private String inrixVendorId;
	@Value("${inrix.consumer.id}")
	private String inrixConsumerId;

	// Authentication
	@Value("${wavetronix.api.user}")
	private String wavetronixUser;
	@Value("${wavetronix.api.password}")
	private String wavetronixPass;

	// Archive Folder Variables
	@Value("${archive.root.folder}")
	private String archiveRoot;
	@Value("${archive.iadot.root.folder}")
	private String dotArchiveRoot;

	public String getDotArchiveRealtimeFolder() {
		return dotArchiveRealtimeFolder;
	}

	public String getDotArchivePerdayFolder() {
		return dotArchivePerdayFolder;
	}

	@Value("${archive.iadot.realtimedata.folder}")
	private String dotArchiveRealtimeFolder;
	@Value("${archive.iadot.perdaydata.folder}")
	private String dotArchivePerdayFolder;

	@Value("${archive.iadot.smoothingdata.root.folder}")
	private String dotSmoothingArchiveRoot;

	@Value("${archive.iadot.smoothingdata.realtimedata.folder}")
	private String dotSmoothingArchiveRealtimeFolder;
	@Value("${archive.iadot.smoothingdata.perdaydata.folder}")
	private String dotSmoothingArchivePerdayFolder;

	public String getDotSmoothingArchiveRoot() {
		return dotSmoothingArchiveRoot;
	}

	public String getDotSmoothingArchiveRealtimeFolder() {
		return dotSmoothingArchiveRealtimeFolder;
	}

	public String getDotSmoothingArchivePerdayFolder() {
		return dotSmoothingArchivePerdayFolder;
	}

	public String getDecisionTreeModelPath() {
		return decisionTreeModelPath;
	}

	@Value("${archive.iadot.smoothingdata.model}")
	private String decisionTreeModelPath;

	public String getDotXmlPath() {
		return dotXmlPath;
	}

	@Value("${archive.iadot.xmlpath}")
	private String dotXmlPath;

	@Value("${archive.here.root.folder}")
	private String hereArchiveRoot;
	@Value("${archive.waze.root.folder}")
	private String wazeArchiveRoot;
	@Value("${archive.inrix.root.folder}")
	private String inrixArchiveRoot;

    public String getInrixArchiveRealtimedataFolder() {
        return inrixArchiveRealtimedataFolder;
    }

    public String getInrixArchivePerdaydataFolder() {
        return inrixArchivePerdaydataFolder;
    }

    @Value("${archive.inrix.realtimedata.folder}")
    private String inrixArchiveRealtimedataFolder;
    @Value("${archive.inrix.perdaydata.folder}")
    private String inrixArchivePerdaydataFolder;
	@Value("${archive.snowplow.root.folder}")
	private String snowPlowArchiveRoot;

	// Archive File Variables
	@Value("${archive.file.iadot.trafficData.name}")
	private String trafficDataFNPrefix;
	@Value("${archive.file.iadot.sensorData.name}")
	private String sensorDataFNPrefix;
	@Value("${archive.file.iadot.workzoneData.name}")
	private String workzoneDataFNPrefix;
	@Value("${archive.file.here.trafficIncident.name}")
	private String incidentFNPrefix;
	@Value("${archive.file.here.realTimeFlow.name}")
	private String realTimeFlowFNPrefix;
	@Value("${archive.file.waze.rss.name}")
	private String wazeFeedFNPrefix;
	@Value("${archive.file.inrix.name}")
	private String inrixFeedFNPrefix;
	@Value("${archive.file.snowplow.name}")
	private String snowPlowFNPrefix;

	// Twilio SMS Variables
	@Value("${sms.twilio.host}")
	private String twilioHost;
	@Value("${sms.twilio.sid}")
	private String twilioSid;
	@Value("${sms.twilio.authtoken}")
	private String twilioAuthToken;
	@Value("${sms.twilio.from}")
	private String twilioFrom;

	public String getInrixDataLakeRoot() {
		return inrixDataLakeRoot;
	}

	public void setInrixDataLakeRoot(String inrixDataLakeRoot) {
		this.inrixDataLakeRoot = inrixDataLakeRoot;
	}

	@Value("${datalake.inrix.root.folder}")
	private String inrixDataLakeRoot;

    public String getInrixReplaceFile() {
        return inrixReplaceFile;
    }

    @Value("${inrix.replace.file}")
    private String inrixReplaceFile;

	public String getIadotReplaceFile() {
		return iadotReplaceFile;
	}

	public void setIadotReplaceFile(String iadotReplaceFile) {
		this.iadotReplaceFile = iadotReplaceFile;
	}

	@Value("${iadot.replace.file}")
	private String iadotReplaceFile;

	@Value("${inrix.perdaydata}")
	private String inrixPerdaydata;

	public String getInrixPerdaydata() {
		return inrixPerdaydata;
	}

	public String getIadotPerdaydata() {
		return iadotPerdaydata;
	}

	@Value("${iadot.perdaydata}")
	private String iadotPerdaydata;

	public String getIadotSmoothingdataPerdaydata() {
		return iadotSmoothingdataPerdaydata;
	}

	@Value("${iadot.smoothingdata.perdaydata}")
	private String iadotSmoothingdataPerdaydata;



	public Boolean isCloud() {
		return Boolean.valueOf(cloud);
	}

	public String getTrafficDataReqUrl() {
		return trafficDataReqUrl;
	}

	public String getSensorInventoryReqUrl() {
		return sensorInventoryReqUrl;
	}

	public String getMsgTrafficDataReqParam() {
		return msgTrafficDataReqParam;
	}

	public String getMsgInventoryReqParam() {
		return msgInventoryReqParam;
	}

	public String getTrafficIncidentUrl() {
		return trafficIncidentUrl;
	}

	public String getCameraInventoryUrl() {
		return cameraInventoryUrl;
	}

	public String getRealTimeFlowUrl() {
		return realTimeFlowUrl;
	}

	public String getIowaDOTUsername() {
		return iowaDOTUsername;
	}

	public String getIowaDOTPassowrd() {
		return iowaDOTPassowrd;
	}

	public String getWazeFeedXmlurl() {
		return wazeFeedXmlurl;
	}

	public String getWazeFeedJsonUrl() {
		return wazeFeedJsonUrl;
	}

	public String getWavetronixUser() {
		return wavetronixUser;
	}

	public String getInrixBaseUrl() {
		return inrixBaseUrl;
	}

	public String getInrixVendorId() {
		return inrixVendorId;
	}

	public String getInrixConsumerId() {
		return inrixConsumerId;
	}

	public String getWavetronixPass() {
		return wavetronixPass;
	}

	public String getArchiveRoot() {
		return archiveRoot;
	}

	public String getDotArchiveRoot() {
		return dotArchiveRoot;
	}

	public String getHereArchiveRoot() {
		return hereArchiveRoot;
	}

	public String getWazeArchiveRoot() {
		return wazeArchiveRoot;
	}

	public String getTrafficDataFNPrefix() {
		return trafficDataFNPrefix;
	}

	public String getSensorDataFNPrefix() {
		return sensorDataFNPrefix;
	}

	public String getWorkzoneDataFNPrefix() {
		return workzoneDataFNPrefix;
	}

	public String getIncidentFNPrefix() {
		return incidentFNPrefix;
	}

	public String getRealTimeFlowFNPrefix() {
		return realTimeFlowFNPrefix;
	}

	public String getWazeFeedFNPrefix() {
		return wazeFeedFNPrefix;
	}

	public String getInrixArchiveRoot() {
		return inrixArchiveRoot;
	}

	public String getInrixFeedFNPrefix() {
		return inrixFeedFNPrefix;
	}

	public String getTwilioHost() {
		return twilioHost;
	}

	public String getTwilioSid() {
		return twilioSid;
	}

	public String getTwilioAuthToken() {
		return twilioAuthToken;
	}

	public String getTwilioFrom() {
		return twilioFrom;
	}

	public String getDmsInventoryUrl() {
		return dmsInventoryUrl;
	}

	public String getSnowPlowArchiveRoot() {
		return snowPlowArchiveRoot;
	}

	public String getSnowPlowFNPrefix() {
		return snowPlowFNPrefix;
	}
	
	

}
