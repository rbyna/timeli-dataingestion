package com.intrans.timeli.dataingestion.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;

import java.util.Collection;
import java.util.List;

@RequestMapping(value = { "/api" })
@Controller
public class RedisDataController {

    @RequestMapping(value = "/getNearBySegments", method = { RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<Collection<String>> getIncidents(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        try {
            String key = code + "L";
            Jedis jedis = new Jedis("23.101.126.159", 6379);
            jedis.auth("Clust3rax$");
            long count = jedis.llen(key);
            List<String> codes = jedis.lrange(key, 0, count - 1);
            ResponseEntity<Collection<String>> responseEntity =
                    new ResponseEntity<Collection<String>>(codes, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);

            return responseEntity;
        } catch (Exception e) {
            System.out.println("Failed to get incidents");
        }
        return null;
    }

}