package com.intrans.timeli.dataingestion.entities;

import com.intrans.timeli.dataingestion.utils.DateUtils;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="dms_inventory")
public class DMSInventory {


	public GeoJsonPoint getGeometry() {
		return geometry;
	}

	public void setGeometry(GeoJsonPoint geometry) {
		this.geometry = geometry;
	}

	@Indexed
	private GeoJsonPoint geometry;
	@Indexed
	private Long deviceId;
	@Indexed
	private String deviceName;
	private String organizationId;
	private String organizationName;
	private String longitude;
	private String lattitude;
	private String networkId;
	private String linkDirection;
	private String linearReference;
	private String routeDesignator;
	private String signType;
	private String signTechnology;
	private String signHeight;
	private String signWidth;
	private String charHeight;
	private String charWidth;
	private String beaconType;
	private String lastUpdateTime;
	private Long archiveTime = DateUtils.getCurrentTime();


	public Long getArchiveTime() {
		return archiveTime;
	}

	public void setArchiveTime(Long archiveTime) {
		this.archiveTime = archiveTime;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getLinkDirection() {
		return linkDirection;
	}

	public void setLinkDirection(String linkDirection) {
		this.linkDirection = linkDirection;
	}

	public String getLinearReference() {
		return linearReference;
	}

	public void setLinearReference(String linearReference) {
		this.linearReference = linearReference;
	}

	public String getRouteDesignator() {
		return routeDesignator;
	}

	public void setRouteDesignator(String routeDesignator) {
		this.routeDesignator = routeDesignator;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getSignTechnology() {
		return signTechnology;
	}

	public void setSignTechnology(String signTechnology) {
		this.signTechnology = signTechnology;
	}

	public String getSignHeight() {
		return signHeight;
	}

	public void setSignHeight(String signHeight) {
		this.signHeight = signHeight;
	}

	public String getSignWidth() {
		return signWidth;
	}

	public void setSignWidth(String signWidth) {
		this.signWidth = signWidth;
	}

	public String getCharHeight() {
		return charHeight;
	}

	public void setCharHeight(String charHeight) {
		this.charHeight = charHeight;
	}

	public String getCharWidth() {
		return charWidth;
	}

	public void setCharWidth(String charWidth) {
		this.charWidth = charWidth;
	}

	public String getBeaconType() {
		return beaconType;
	}

	public void setBeaconType(String beaconType) {
		this.beaconType = beaconType;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

}
