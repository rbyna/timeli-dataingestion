package com.intrans.timeli.dataingestion.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.intrans.timeli.dataingestion.utils.DateUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author krishnaj
 *
 */

@Document(collection="dmsInventoryArchive")
public class DMSInventories {
	@Id
	@JsonIgnore
	private String id;
	private List<DMSInventory> dmsInventoryList = new ArrayList<>();
	private Long archiveTime = DateUtils.getCurrentTime();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<DMSInventory> getDmsInventoryList() {
		return dmsInventoryList;
	}

	public void setDmsInventoryList(List<DMSInventory> dmsInventoryList) {
		this.dmsInventoryList = dmsInventoryList;
	}

	public Long getArchiveTime() {
		return archiveTime;
	}

	public void setArchiveTime(Long archiveTime) {
		this.archiveTime = archiveTime;
	}

}
