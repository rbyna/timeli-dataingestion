package com.intrans.timeli.dataingestion.process;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import weka.classifiers.Classifier;
import weka.core.Instances;



public class TIMELIPipeLineSmoothing {
	//detector-ID, local-date, start-time, end-time, timeStartSec, timeEndSec, volume, occu/numberOfLanes, speed/vol*0.621371, smoothedOcc, smoothedSpeed, predictedMsg
//	private static String FOLDERPATH = "/Users/ravitejarajabyna/ra/datasource/datadrive2/iadot/smoothingdata";
//	private static String FOLDERPATH_PERDAY = "/Users/ravitejarajabyna/ra/datasource/datadrive2/iadot/smoothingdata/perdaydata";
	
//	public static void smoothing(String xmlPath, HashMap<String, TIMELISensorClass> IWZCLassMap, String todayFile) throws Exception {
	public static Object[] smoothing(String xmlPath, HashMap<String, TIMELISensorClass> IWZCLassMap, 
			String todayFile, ArrayList<String> sensorIDs, String realtimeFolderPath, String perdayFolderPath, String modelPath, boolean perdaydataRequired) throws Exception {
		Classifier cls = (Classifier) weka.core.SerializationHelper.read(modelPath);
//		String todayFile = "20180401";
		String today = new SimpleDateFormat("yyyyMMdd_hhmmss").format(new java.util.Date());
		String today2 = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
		
//		ArrayList<String> sensorIDs = new ArrayList<>();
		if(!todayFile.equals(today2)) {
			todayFile = today;
			
			//update the sensorID everyday midnight;

		sensorIDs = TIMELIUtilityMethods.getSensorIDList(xmlPath);
			
			IWZCLassMap = new HashMap<>();
			// build the Map and initialize a IWZ class for each sensorID
			for (String id : sensorIDs){
//				System.out.println(id);
				TIMELISensorClass temp = new TIMELISensorClass(id);
				IWZCLassMap.put(id, temp);
			}
		}
		
		
		String filePath = today2 + ".csv";
		File file = new File(filePath);
		file.deleteOnExit();
		FileWriter fw = new FileWriter(realtimeFolderPath + "/" + filePath, false);
		FileWriter fw_perday = new FileWriter(perdayFolderPath + "/" + filePath, true);
		
		String filePath_snapshot = today2 + "_snapshot.csv";
		FileWriter fw_snapshot = new FileWriter(realtimeFolderPath + "/" + filePath_snapshot);
		
		HashMap<String, String> XML_info = TIMELIUtilityMethods.getDetectorsInfo(sensorIDs, xmlPath);
			
		for (String id: XML_info.keySet()){	
			// extract the traffic info for each sensor id
			String idInfo = XML_info.get(id);
			if(idInfo.contains("-1")){
				continue;
			}
			
			// update the sensor class for each class
			TIMELISensorClass iwzSensor = IWZCLassMap.get(id);
			
			//date, startTime, endTime, timeStartSec, timeEndSec, vol, occu, speed;
			String time  = idInfo.split(",")[3]; //start time in seconds
			String speed = idInfo.split(",")[7]; //speed
			String occu  = idInfo.split(",")[6]; //occu
            String status  = idInfo.split(",")[8]; //status
			String infoDate = idInfo.split(",")[0];
			String infoStartTime = idInfo.split(",")[1]; //start time in HHmmss
			
			iwzSensor.fillArrays(speed, occu, time);
			
			boolean initStatus = iwzSensor.getInitStatus();
			
			if (initStatus) {
				double smootedSpeed = iwzSensor.getSmoothSpeed();
				double smootedOccu  = iwzSensor.getSmoothOccupancy();
				
				Instances newData = TIMELIUtilityMethods.buildDataSet(smootedSpeed, smootedOccu);
				String predMsg = TIMELIUtilityMethods.revisedDT(newData, cls);
				
				String log = id + ", " + idInfo + ", " + smootedOccu + ", " + smootedSpeed + ", " + predMsg;
//				System.out.println(log);
				
				// Write to the historical database
				fw.write(log + "\n");

				fw.flush();
                if (perdaydataRequired) {
                    fw_perday.write(log + "\n");
                }
                fw_perday.flush();
				// Write to the snap_shot
				fw_snapshot.write(log + "\n");	
				fw_snapshot.flush();
				
			} else {
//				System.out.println(id + ": Initializing, " + idInfo);
			}
		}
		//write object to the file
		
		
//		System.out.println("--------------------------------------xml is processed and saved-------------------------------");
		fw.close();		
		fw_snapshot.close();
		fw_perday.close();


		
		Object [] ans = {IWZCLassMap, sensorIDs};

		return ans;
	}
}
