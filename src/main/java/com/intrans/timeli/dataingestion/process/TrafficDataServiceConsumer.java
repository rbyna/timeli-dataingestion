package com.intrans.timeli.dataingestion.process;

import com.intrans.timeli.dataingestion.constants.ConfigConstants;
import com.intrans.timeli.dataingestion.dto.InrixAuthToken;
import com.intrans.timeli.dataingestion.marshalling.DetectorInventory;
import com.intrans.timeli.dataingestion.marshalling.TrafficDetectorData;
import com.intrans.timeli.dataingestion.utils.DateUtils;
import com.intrans.timeli.dataingestion.utils.TimeliUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Arrays;

/**
 * This consumer class consumes data from different end points and returns
 * response.
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
@Component
public class TrafficDataServiceConsumer {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ConfigConstants configConstants;

	private String reqMsg = "wavetronix";

	private InrixAuthToken inrixAuthToken = new InrixAuthToken();

	/**
	 * Consumes Shared Traffic Data feed from DOT with GET Request.
	 * 
	 * @return {@link TrafficDetectorData}
	 */
	public TrafficDetectorData getTrafficData() {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configConstants.getTrafficDataReqUrl())
				.queryParam(configConstants.getMsgTrafficDataReqParam(), reqMsg);
		return restTemplate.getForObject(uriBuilder.build().encode().toUriString(), TrafficDetectorData.class);
	}

	/**
	 * Consumes Shared Traffic Data feed from DOT with POST Request.
	 * 
	 * @return {@link TrafficDetectorData}
	 */
	public TrafficDetectorData getTrafficDataWithPostReq() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> bodyParams = new LinkedMultiValueMap<String, String>();
		bodyParams.put(configConstants.getMsgTrafficDataReqParam(), Arrays.asList(reqMsg));
		HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<MultiValueMap<String, String>>(bodyParams,
				headers);
		TrafficDetectorData data = null;
		try {
			data = restTemplate.postForObject(configConstants.getTrafficDataReqUrl(), httpEntity,
					TrafficDetectorData.class);
		} catch (HttpClientErrorException ex) {
			// TODO Logging
			System.out.println(ex.getResponseBodyAsString());
		}
		return data;
	}

	/**
	 * Consumes Shared Sensor Inventory feed from DOT with GET Request.
	 * 
	 * @return {@link DetectorInventory}
	 */
	@Caching(evict = { @CacheEvict("sensorInventory") }, put = { @CachePut("sensorInventory") })
	public DetectorInventory getSensorInventoryData() {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configConstants.getSensorInventoryReqUrl())
				.queryParam(configConstants.getMsgInventoryReqParam(), "UseLocationReference");
		return restTemplate.getForObject(uriBuilder.build().encode().toUriString(), DetectorInventory.class);
	}

	/**
	 * Consumes Shared Sensor Inventory feed from DOT with POST Request.
	 * 
	 * @return {@link DetectorInventory}
	 */
	public DetectorInventory getSensorInventoryDataWithPostReq() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> bodyParams = new LinkedMultiValueMap<String, String>();
		bodyParams.put(configConstants.getMsgInventoryReqParam(), Arrays.asList("UseLocationReference"));
		HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<MultiValueMap<String, String>>(bodyParams,
				headers);
		DetectorInventory data = null;
		try {
			data = restTemplate.postForObject(configConstants.getSensorInventoryReqUrl(), httpEntity,
					DetectorInventory.class);
		} catch (HttpClientErrorException ex) {
			// TODO Logging
			System.out.println(ex.getResponseBodyAsString());
		}
		return data;
	}

	/**
	 * Consumes traffic incident data.
	 * 
	 * @return
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws RestClientException
	 */
	public ResponseEntity<byte[]> getTrafficIncidentData() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getInterceptors().add(
				getBasicAuthInterceptor(configConstants.getIowaDOTUsername(), configConstants.getIowaDOTPassowrd()));
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		return restTemplate.exchange(configConstants.getTrafficIncidentUrl(), HttpMethod.GET,
				new HttpEntity<String>(headers), byte[].class);
	}

	/**
	 * Consumes traffic real time data.
	 * 
	 * @return
	 */
	public ResponseEntity<byte[]> getRealTimeFlowData() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getInterceptors().add(
				getBasicAuthInterceptor(configConstants.getIowaDOTUsername(), configConstants.getIowaDOTPassowrd()));
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		return restTemplate.exchange(configConstants.getRealTimeFlowUrl(), HttpMethod.GET,
				new HttpEntity<String>(headers), byte[].class);
	}

	/**
	 * Returns Waze feed in XML or JSON format. If the provided format is not
	 * recongnized then defaults the data in XML format.
	 * 
	 * @param mediaType
	 * @return
	 */
	public ResponseEntity<String> getWazeFeed(MediaType mediaType) {
		String url = configConstants.getWazeFeedXmlurl();
		if (mediaType == MediaType.APPLICATION_JSON) {
			url = configConstants.getWazeFeedJsonUrl();
		}
		return restTemplate.exchange(url, HttpMethod.GET, null, String.class);
	}

	/**
	 * Returns Waze feed in XML format.
	 * 
	 * @return
	 */
	public ResponseEntity<String> getXMLWazeFeed() {
		return getWazeFeed(MediaType.TEXT_XML);
	}

	/**
	 * Returns Inrix Feed
	 * 
	 * @return
	 * @throws ParseException
	 */
	public ResponseEntity<String> getInrixFeed() throws ParseException {
		MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<String, String>();
		urlParams.add("action", "getsegmentspeedingeography");
		urlParams.add("geoid", "249");
		urlParams.add("SpeedOutputFields", "Speed,Average,Reference,Score,TTM,Confidence");
		// Update authToken when expiry time is in 10 minutes
		if (StringUtils.isAnyBlank(inrixAuthToken.getAuthToken(), inrixAuthToken.getExpiryTimeStr())
				|| DateUtils.addMinutes(DateUtils.getCurrentDate(), 10).compareTo(inrixAuthToken.getExpiryTime()) > 0) {
			updateInrixAuthToken(inrixAuthToken);
		}
		urlParams.add("token", inrixAuthToken.getAuthToken());
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configConstants.getInrixBaseUrl())
				.queryParams(urlParams);
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = restTemplate.getForEntity(uriBuilder.build().toUriString(), String.class);
		} catch (HttpClientErrorException ex) {
			// Try to update auth token
			updateInrixAuthToken(inrixAuthToken);
			responseEntity = restTemplate.getForEntity(uriBuilder.build().toUriString(), String.class);
		}

		return responseEntity;
	}

	/**
	 * Returns Camera Inventory Information.
	 * 
	 * @return
	 */
	public ResponseEntity<String> getCameraInventory() {
		try {
			return restTemplate.exchange(configConstants.getCameraInventoryUrl(), HttpMethod.GET, null, String.class);
		} catch (HttpClientErrorException ex) {
			System.out.println(ex.getResponseBodyAsString());
		}
		return null;
	}

	/**
	 * Returns DMS Inventory Information.
	 * 
	 * @return
	 */
	public ResponseEntity<String> getDMSInvenotry() {
		try {
			return restTemplate.getForEntity(configConstants.getDmsInventoryUrl(), String.class);
		} catch (HttpClientErrorException ex) {
			System.out.println(ex.getResponseBodyAsString());
		}
		return null;
	}

	/**
	 * Updates Inrix AuthToken and Expiry Time
	 */
	public void updateInrixAuthToken(InrixAuthToken inrixAuthToken) {
		MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<String, String>();
		urlParams.add("action", "getsecuritytoken");
		urlParams.add("Vendorid", configConstants.getInrixVendorId());
		urlParams.add("consumerid", configConstants.getInrixConsumerId());
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configConstants.getInrixBaseUrl())
				.queryParams(urlParams);
		ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.build().toUriString(), String.class,
				urlParams);
		if (response.getStatusCode() == HttpStatus.OK) {
			try {
				Document doc = TimeliUtils.parseXML(response.getBody());
				NodeList authNodes = doc.getElementsByTagName("AuthToken");
				if (authNodes.getLength() > 0) {
					Node authNode = authNodes.item(0);
					Node expiryNode = authNode.getAttributes().getNamedItem("expiry");
					inrixAuthToken.setAuthToken(authNode.getTextContent());
					inrixAuthToken.setExpiryTime(expiryNode.getTextContent());
				}
			} catch (Exception e) {
				// TODO Log
			}
		}
	}

	/**
	 * Creates basic authentication interceptor.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	private ClientHttpRequestInterceptor getBasicAuthInterceptor(String username, String password) {
		return new BasicAuthorizationInterceptor(username, password);
	}

}
