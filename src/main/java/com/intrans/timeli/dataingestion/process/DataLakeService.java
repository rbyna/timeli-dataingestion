package com.intrans.timeli.dataingestion.process;

import com.microsoft.azure.datalake.store.ADLStoreClient;
import com.microsoft.azure.datalake.store.oauth2.AccessTokenProvider;
import com.microsoft.azure.datalake.store.oauth2.ClientCredsTokenProvider;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class DataLakeService {

    private ADLStoreClient client;
    private static String accountFQDN = "hadoopbatch.azuredatalakestore.net";  // full account FQDN, not just the account name
    private static String clientId = "b7c45e99-47d2-4fc1-a5ef-75bdd5b2f30d";
    private static String authTokenEndpoint = "https://login.microsoftonline.com/c62c238f-6e50-4ad6-905c-4fd61b5e3654/oauth2/token";
    private static String clientKey = "WAfXEgmcSmsTdJgj0zkRdGFBT7uJ/lcIUvpjVKi+XNU=";

    @PostConstruct
    public void runOnStartup() throws IOException {
        initializeAzureDataLakeStore();
    }

    private void initializeAzureDataLakeStore() throws IOException {
        AccessTokenProvider provider = new ClientCredsTokenProvider(authTokenEndpoint, clientId, clientKey);
//            AccessTokenProvider provider = new DeviceCodeTokenProvider("b7c45e99-47d2-4fc1-a5ef-75bdd5b2f30d");
        this.client = ADLStoreClient.createClient(accountFQDN, provider);

    }

    public ADLStoreClient getClient() {
        return this.client;
    }

}
