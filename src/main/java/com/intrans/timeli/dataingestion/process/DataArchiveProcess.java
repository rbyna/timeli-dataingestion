package com.intrans.timeli.dataingestion.process;

import com.intrans.timeli.dataingestion.constants.ConfigConstants;
import com.intrans.timeli.dataingestion.convertors.XML2CSVConverter;
import com.intrans.timeli.dataingestion.entities.CameraInventory;
import com.intrans.timeli.dataingestion.entities.DMSInventory;
import com.intrans.timeli.dataingestion.marshalling.TrafficDetectorData;
import com.intrans.timeli.dataingestion.repository.CameraInventoryDao;
import com.intrans.timeli.dataingestion.repository.DMSInventoryDao;
import com.intrans.timeli.dataingestion.repository.RawInrixDataDao;
import com.intrans.timeli.dataingestion.utils.*;
import com.microsoft.azure.datalake.store.ADLFileOutputStream;
import com.microsoft.azure.datalake.store.ADLStoreClient;
import com.microsoft.azure.datalake.store.IfExists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class DataArchiveProcess {

    @Autowired
    private TrafficDataServiceConsumer trafficDataServiceConsumer;
    @Autowired
    private ConfigConstants configConstants;
    @Autowired
    private DMSInventoryDao dmsInventoryDao;
    @Autowired
    private CameraInventoryDao cameraInventoryDao;

    @Autowired
    private RawInrixDataDao rawInrixDataDao;

    @Autowired
    private DataLakeService dataLakeService;

    private final String trafficDetectorDataXSL = "TrafficDetectorData.xsl";
    private final String inrixFeedXSL = "InrixFeed.xsl";

    private static HashMap<String, TIMELISensorClass> IWZClassMap;

    private static String todayFile = "";
    ArrayList<String> sensorIDs = new ArrayList<>();
    private static String xmlPath = "/Users/ravitejarajabyna/ra/datasource/datadrive2/iadot/trafficdata.xml";

	/*@PostConstruct
	public void setUpFolders() throws IOException {
		Path archivePath = Paths.get(configConstants.getArchiveRoot());
		if (!Files.exists(archivePath)) {
			FileUtils.makeDirectory(archivePath);
		}
		Path iaDotArchivePath = Paths.get(configConstants.getDotArchiveRoot());
		if (!Files.exists(iaDotArchivePath)) {
			FileUtils.makeDirectory(iaDotArchivePath);
		}
//		Path hereArchivePath = Paths.get(configConstants.getHereArchiveRoot());
//		if (!Files.exists(hereArchivePath)) {
//			FileUtils.makeDirectory(hereArchivePath);
//		}
//		Path wazeArchivePath = Paths.get(configConstants.getWazeArchiveRoot());
//		if (!Files.exists(wazeArchivePath)) {
//			FileUtils.makeDirectory(wazeArchivePath);
//		}
		Path inrixArchivePath = Paths.get(configConstants.getInrixArchiveRoot());
		if (!Files.exists(inrixArchivePath)) {
			FileUtils.makeDirectory(inrixArchivePath);
		}
	}*/

    /**
     * Archives Traffic Detector Data into a file as CSV format.
     *
     * @param method
     * @throws Exception
     */
    public void archiveTrafficDetectorData(HttpMethod method) throws Exception {
        final String xmlPath = configConstants.getDotXmlPath();
        TrafficDetectorData trafficDetectorData = null;
        if (method == HttpMethod.GET) {
            trafficDetectorData = trafficDataServiceConsumer.getTrafficData();
        } else if (method == HttpMethod.POST) {
            trafficDetectorData = trafficDataServiceConsumer.getTrafficDataWithPostReq();
        }
        // Handover to the joint feed handler.
//		final TrafficDetectorData tdd = trafficDetectorData;
//		new AsyncCallBack(new Callback() {
//
//			@Override
//			public void call() {
//				try {
//					sensorFeedHandler.updateSensorFeed(tdd);
//				} catch (IOException e) {
//					System.out.println("Error while updating the sensor joint feed.");
//					e.printStackTrace();
//				} catch (Exception e) {
//					System.out.println("Error while updating the sensor joint feed.");
//					e.printStackTrace();
//				}
//
//			}
//		}).execute(taskExecutor);



        File internalFile = FileUtils.archiveDataForInternal(configConstants.getDotArchiveRoot(),
                configConstants.getTrafficDataFNPrefix(), trafficDetectorData);
        long startTime = System.currentTimeMillis();
//        System.out.println("Start smoothing : " + new Date());
        Object [] historyObjs = TIMELIPipeLineSmoothing.smoothing(xmlPath, IWZClassMap, todayFile, sensorIDs, configConstants.getDotSmoothingArchiveRealtimeFolder(), configConstants.getDotSmoothingArchivePerdayFolder(), configConstants.getDecisionTreeModelPath(), Boolean.valueOf(configConstants.getIadotSmoothingdataPerdaydata()));
//        System.out.println("End smoothing : " + new Date());
//        System.out.println("Total time : " + (startTime - System.currentTimeMillis()));
        IWZClassMap = (HashMap<String, TIMELISensorClass>)historyObjs[0];
        sensorIDs   = (ArrayList<String>)historyObjs[1];

        String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
        todayFile = today;

        // Create/Update CSV file.
//        Path toDayDir = FileUtils.getTodayDirectory(configConstants.getDotArchiveRoot());
//        File csvFile = FileUtils.getFileObj(toDayDir.toString(), configConstants.getTrafficDataFNPrefix(),
//                FileUtils.FileType.CSV);
        // Hardcoding header values:(
        String headers = "owner-Id,network-Id,local-date,local-time,utc-offset,start-time,end-time,detector-Id,status,lane-Id,lane-count,lane-volume,"
                + "lane-occupancy,lane-speed,small-class-count,"
                + "small-class-volume,medium-class-count,medium-class-volume,large-class-count,large-class-volume";

        File csvFile = FileUtils.getFile(FileUtils.getTodayDirectory(configConstants.getDotArchiveRoot()).toString(),
                DateUtils.getDefaultCurrentDateTimeStr(), FileUtils.FileType.CSV);
        if (!Boolean.valueOf(configConstants.getIadotReplaceFile())) {
            convertToCsv(csvFile, new FileInputStream(internalFile), headers, trafficDetectorDataXSL);
        }
        csvFile = FileUtils.getFile(configConstants.getDotArchiveRealtimeFolder(),
                configConstants.getTrafficDataFNPrefix(), FileUtils.FileType.CSV);
        if (csvFile.exists()) {
            csvFile.delete();
        }
        convertToCsv(csvFile, new FileInputStream(internalFile), headers, trafficDetectorDataXSL);
        if (Boolean.valueOf(configConstants.getIadotPerdaydata())) {
            csvFile = FileUtils.getFile(configConstants.getDotArchivePerdayFolder(),
                    DateUtils.getDefaultCurrentDateStr(), FileUtils.FileType.CSV);
            convertToCsv(csvFile, new FileInputStream(internalFile), headers, trafficDetectorDataXSL);
        }

    }

    //Note: No xml is generated
    public void archiveInrixFeed() throws Exception {
        ResponseEntity<String> response = trafficDataServiceConsumer.getInrixFeed();
        if (response.getStatusCode() == HttpStatus.OK) {
            String headers = "code,c-value,segmentClosed,score,speed,average,reference,travelTimeMinutes,time";
            File csvFile = FileUtils.getFile(FileUtils.getTodayDirectory(configConstants.getInrixArchiveRoot()).toString(),
                    DateUtils.getDefaultCurrentDateTimeStr(), FileUtils.FileType.CSV);
            if (!Boolean.valueOf(configConstants.getInrixReplaceFile())) {
                convertToCsv(csvFile, response, headers);
            }
            csvFile = FileUtils.getFile(configConstants.getInrixArchiveRealtimedataFolder(),
                    configConstants.getInrixFeedFNPrefix(), FileUtils.FileType.CSV);
            if (csvFile.exists()) {
                csvFile.delete();
            }
            convertToCsv(csvFile, response, headers);
            if (Boolean.valueOf(configConstants.getInrixPerdaydata())) {
                csvFile = FileUtils.getFile(configConstants.getInrixArchivePerdaydataFolder(),
                        DateUtils.getDefaultCurrentDateStr(), FileUtils.FileType.CSV);
                convertToCsv(csvFile, response, headers);
            }


        }

    }

    public void deleteData() throws Exception {
        Path yesterdayDirPath = FileUtils.getYesterdayDirectory(configConstants.getInrixArchivePerdaydataFolder());
        File yesterdayDir = new File(yesterdayDirPath.toString());
        String[] entries = yesterdayDir.list();
        for (String s : entries) {
            File currentFile = new File(yesterdayDir.getPath(), s);
            currentFile.delete();
        }
        yesterdayDir.delete();
    }

    public void deleteRawInrixData() throws Exception {

//        rawInrixDataDao.deleteAll();
//        Integer integer = rawInrixDataDao.queryByTimestamp(getPreviousTime(120), getPreviousTime(119));
//        System.out.println(integer);
    }

    public static long getPreviousTime(int amount) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, (-1) * amount);
        return cal.getTime().getTime();
    }

    private void convertToCsv(File csvFile, ResponseEntity<String> response, String headers) throws Exception {
        if (!csvFile.exists() || csvFile.isDirectory()) {
            TimeliUtils.updateCSVFileWithHeaders(csvFile, headers);
        }
        ClassPathResource styleSheetStream = new ClassPathResource(inrixFeedXSL);

        // Convert XML data into CSV and write into the file.
        XML2CSVConverter.convert(new ByteArrayInputStream(response.getBody().getBytes()),
                styleSheetStream.getInputStream(), csvFile);
    }

    private void convertToCsv(File csvFile, InputStream inputStream, String headers, String xsl) throws Exception {
        if (!csvFile.exists() || csvFile.isDirectory()) {
            TimeliUtils.updateCSVFileWithHeaders(csvFile, headers);
        }
        ClassPathResource styleSheetStream = new ClassPathResource(xsl);

        // Convert XML data into CSV and write into the file.
        XML2CSVConverter.convert(inputStream, styleSheetStream.getInputStream(), csvFile);
    }

    public void archiveInrixBatchDataInDataLake() throws Exception {
        ADLStoreClient client = dataLakeService.getClient();
        ResponseEntity<String> response = trafficDataServiceConsumer.getInrixFeed();
        if (response.getStatusCode() == HttpStatus.OK) {
            Path todayDir = FileUtils.getTodayDirectory(configConstants.getInrixArchiveRoot());
            File csvFile = FileUtils.getFileObj(todayDir.toString(), configConstants.getInrixFeedFNPrefix(),
                    FileUtils.FileType.CSV);
            // Hardcoding header values:(
            String headers = "code,c-value,segmentClosed,score,speed,average,reference,travelTimeMinutes,time";
            if (csvFile.exists()) {
                csvFile.delete();
            }
            if (!csvFile.exists() || csvFile.isDirectory()) {
                TimeliUtils.updateCSVFileWithHeaders(csvFile, headers);
            }
            ClassPathResource styleSheetStream = new ClassPathResource(inrixFeedXSL);
            String dataLakeFileName = getDataLakeFileName(client);
            if (!client.checkExists(dataLakeFileName)) {
//			TimeliUtils.updateCSVFileWithHeaders(csvFile, headers);
                ADLFileOutputStream stream = client.createFile(dataLakeFileName, IfExists.OVERWRITE);
                client.setPermission(dataLakeFileName, "744");
                PrintStream out = new PrintStream(stream);
                out.println(headers);
                out.close();
                stream.close();
            }
            // Convert XML data into CSV and write into the file.
            XML2CSVConverter.convert(new ByteArrayInputStream(response.getBody().getBytes()),
                    styleSheetStream.getInputStream(), csvFile, dataLakeFileName, client);

        }
    }

    private String getDataLakeFileName(ADLStoreClient client) throws IOException {
        Path todayDir = DataLakeUtils.getTodayDirectory(configConstants.getInrixDataLakeRoot(), client);
        File csvFile = FileUtils.getFileObj(todayDir.toString(), configConstants.getInrixFeedFNPrefix(),
                FileUtils.FileType.CSV);
        String csvFilename = csvFile.getAbsolutePath();

//		System.out.println(csvFilename);
        return csvFilename;
    }


    /**
     * Archives DMS Inventory information everyday.
     *
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public void archiveDMSInventory() throws ParserConfigurationException, SAXException, IOException {
        ResponseEntity<String> resposne = trafficDataServiceConsumer.getDMSInvenotry();
        if (resposne != null && resposne.getStatusCode() == HttpStatus.OK) {
            List<DMSInventory> dmsInventories = TimeliUtils.unmarshallDMSInventories(resposne.getBody());

//			dmsInventories.setArchiveTime(DateUtils.getCurrentTime());
            dmsInventoryDao.deleteAll();
            dmsInventoryDao.save(dmsInventories);
        }
    }


    /**
     * Updates Camera Inventory Information periodically. It gets the camera
     * Inventory information from DOT servers and converts into list of
     * {@link CameraInventory} object format. Then it compares with the information
     * saved in the database. New Camera information will be inserted. Already
     * existing camera informtion will be updated.
     *
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws ParseException
     */
    public void archiveCameraInventoryInformation()
            throws ParserConfigurationException, SAXException, IOException, ParseException {
        ResponseEntity<String> response = trafficDataServiceConsumer.getCameraInventory();
        List<CameraInventory> cameraInventoryList = TimeliUtils.unmarshallCameraInventory(response.getBody());
        cameraInventoryDao.deleteAll();
        cameraInventoryDao.save(cameraInventoryList);
    }

    public static void main(String[] args) {

    }
}
