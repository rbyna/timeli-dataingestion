package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class DetectorApproach {
	@JacksonXmlProperty(localName = "approach-direction")
	protected String approachDirection;
	@JacksonXmlProperty(localName = "approach-name")
	protected String approachName;
	@JacksonXmlProperty(localName = "lanes-type")
	protected String lanesType;
	@JacksonXmlElementWrapper(localName = "detection-lanes")
	@JacksonXmlProperty(localName = "detection-lane")
	protected List<DetectionLane> detectionLanes;

	public DetectorApproach() {
		super();
	}

	// Stupid hack done to bypass the unsanitized data coming from DOT xml feed.
	public DetectorApproach(String str) {
		super();
	}

	public String getApproachDirection() {
		return approachDirection;
	}

	public void setApproachDirection(String approachDirection) {
		this.approachDirection = approachDirection;
	}

	public String getApproachName() {
		return approachName;
	}

	public void setApproachName(String approachName) {
		this.approachName = approachName;
	}

	public String getLanesType() {
		return lanesType;
	}

	public void setLanesType(String lanesType) {
		this.lanesType = lanesType;
	}

	public List<DetectionLane> getDetectionLanes() {
		if (this.detectionLanes == null)
			this.detectionLanes = new ArrayList<DetectionLane>();
		return detectionLanes;
	}

	public void setDetectionLanes(List<DetectionLane> detectionLanes) {
		this.detectionLanes = detectionLanes;
	}

}
