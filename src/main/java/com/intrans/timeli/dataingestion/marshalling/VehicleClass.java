package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class VehicleClass {

	@JacksonXmlProperty(localName = "class-id")
	protected String classId;
	@JacksonXmlProperty(localName = "count")
	protected Integer count=0;
	@JacksonXmlProperty(localName = "volume")
	protected Integer volume=0;

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

}
