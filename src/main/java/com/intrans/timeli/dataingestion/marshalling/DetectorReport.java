package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class DetectorReport {
	@JacksonXmlProperty(localName = "detector-id")
	protected String detectorId;
	@JacksonXmlProperty(localName = "status")
	protected String status;
	@JacksonXmlElementWrapper(localName = "lanes")
	@JacksonXmlProperty(localName = "lane")
	protected List<RouteLane> routeLanes;

	public String getDetectorId() {
		return detectorId;
	}

	public void setDetectorId(String detectorId) {
		this.detectorId = detectorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RouteLane> getRouteLanes() {
		if (this.routeLanes == null) {
			this.routeLanes = new ArrayList<RouteLane>();
		}
		return routeLanes;
	}

	public void setRouteLanes(List<RouteLane> routeLanes) {
		this.routeLanes = routeLanes;
	}

}
