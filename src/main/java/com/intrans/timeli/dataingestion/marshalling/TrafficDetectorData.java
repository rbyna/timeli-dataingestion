package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "trafficDetectorData")
public class TrafficDetectorData {
	@JacksonXmlProperty(localName = "owner-id")
	protected String ownerId;
	@JacksonXmlProperty(localName = "network-id")
	protected String newtworkId;
	@JacksonXmlElementWrapper(localName = "collection-periods")
	@JacksonXmlProperty(localName = "collection-period")
	protected List<CollectionPeriod> collectionPeriods;

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getNewtworkId() {
		return newtworkId;
	}

	public void setNewtworkId(String newtworkId) {
		this.newtworkId = newtworkId;
	}

	public List<CollectionPeriod> getCollectionPeriods() {
		if (this.collectionPeriods == null) {
			this.collectionPeriods = new ArrayList<CollectionPeriod>();
		}
		return collectionPeriods;
	}

	public void setCollectionPeriods(List<CollectionPeriod> collectionPeriods) {
		this.collectionPeriods = collectionPeriods;
	}

}
