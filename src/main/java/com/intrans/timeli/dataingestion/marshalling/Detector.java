package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class Detector {

	@JacksonXmlProperty(localName = "detector-id")
	protected String detectorId;
	@JacksonXmlProperty(localName = "station-id")
	protected String stationId;
	@JacksonXmlProperty(localName = "detector-name")
	protected String detectorName;
	@JacksonXmlProperty(localName = "detector-location")
	protected DetectorLocation detectorLocation;
	@JacksonXmlProperty(localName = "link-ownership")
	protected String linkOwnership;
	@JacksonXmlProperty(localName = "route-designator")
	protected String routeDesignator;
	@JacksonXmlProperty(localName = "linear-reference")
	protected String linearReference;
	@JacksonXmlProperty(localName = "detector-type")
	protected String detectorType;
	@JacksonXmlElementWrapper(localName = "detector-approaches")
	@JacksonXmlProperty(localName = "detector-approach")
	protected List<DetectorApproach> detectorApproaches;
	@JacksonXmlProperty(localName = "last-update-time")
	protected LastUpdateTime lastUpdateTime;
	//TODO - Right now,we are not getting this information.
	//Is this sensor representing a workzone location?
	protected Boolean workzone;

	public String getDetectorId() {
		return detectorId;
	}

	public void setDetectorId(String detectorId) {
		this.detectorId = detectorId;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getDetectorName() {
		return detectorName;
	}

	public void setDetectorName(String detectorName) {
		this.detectorName = detectorName;
	}

	public DetectorLocation getDetectorLocation() {
		return detectorLocation;
	}

	public void setDetectorLocation(DetectorLocation detectorLocation) {
		this.detectorLocation = detectorLocation;
	}

	public String getLinkOwnership() {
		return linkOwnership;
	}

	public void setLinkOwnership(String linkOwnership) {
		this.linkOwnership = linkOwnership;
	}

	public String getRouteDesignator() {
		return routeDesignator;
	}

	public void setRouteDesignator(String routeDesignator) {
		this.routeDesignator = routeDesignator;
	}

	public String getLinearReference() {
		return linearReference;
	}

	public void setLinearReference(String linearReference) {
		this.linearReference = linearReference;
	}

	public String getDetectorType() {
		return detectorType;
	}

	public void setDetectorType(String detectorType) {
		this.detectorType = detectorType;
	}

	public List<DetectorApproach> getDetectorApproaches() {
		if (this.detectorApproaches == null) {
			this.detectorApproaches = new ArrayList<DetectorApproach>();
		}
		return detectorApproaches;
	}

	public void setDetectorApproaches(List<DetectorApproach> detectorApproaches) {
		this.detectorApproaches = detectorApproaches;
	}

	public LastUpdateTime getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(LastUpdateTime lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public Boolean getWorkzone() {
		return workzone;
	}

	public void setWorkzone(Boolean workzone) {
		this.workzone = workzone;
	}
	
	

}
