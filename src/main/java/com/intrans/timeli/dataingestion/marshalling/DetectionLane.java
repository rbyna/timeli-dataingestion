package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class DetectionLane {

	@JacksonXmlProperty(localName = "lane-id")
	protected String laneId;
	@JacksonXmlProperty(localName = "lane-name")
	protected String laneName;

	public DetectionLane() {
		super();
	}

	// Stupid hack done to bypass the unsanitized data coming from DOT xml feed.
	public DetectionLane(String str) {
		super();
	}

	public String getLaneId() {
		return laneId;
	}

	public void setLaneId(String laneId) {
		this.laneId = laneId;
	}

	public String getLaneName() {
		return laneName;
	}

	public void setLaneName(String laneName) {
		this.laneName = laneName;
	}

}
