package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class RouteLane {
	@JacksonXmlProperty(localName = "lane-id")
	protected Integer laneId;
	@JacksonXmlProperty(localName = "count")
	protected Integer count = 0;
	@JsonProperty(access = Access.WRITE_ONLY)
	@JacksonXmlProperty(localName = "volume")
	protected Integer volume = 0;
	@JacksonXmlProperty(localName = "occupancy")
	protected Integer occupancy = 0;
	@JacksonXmlProperty(localName = "speed")
	protected Integer Speed = 0;
//	@JsonProperty(access = Access.READ_ONLY)
	@JacksonXmlProperty(localName = "small-count")
	protected Integer smallCount = 0;
//	@JsonProperty(access = Access.READ_ONLY)
	@JacksonXmlProperty(localName = "medium-count")
	protected Integer mediumCount = 0;
//	@JsonProperty(access = Access.READ_ONLY)
	@JacksonXmlProperty(localName = "large-count")
	protected Integer largeCount = 0;
//	@JsonProperty(access = Access.WRITE_ONLY)
	@JacksonXmlElementWrapper(localName = "classes")
	@JacksonXmlProperty(localName = "class")
	protected List<VehicleClass> vehicleClasses;
	@JsonIgnore
	private int speedWeight;

	public RouteLane() {
		super();
	}

	// Stupid hack done to bypass the unsanitized data coming from DOT xml feed.
	public RouteLane(String tmp) {
		super();
	}

	public Integer getLaneId() {
		return laneId;
	}

	public void setLaneId(Integer laneId) {
		this.laneId = laneId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Integer getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(Integer occupancy) {
		this.occupancy = occupancy;
	}

	public Integer getSpeed() {
		return Speed;
	}

	public void setSpeed(Integer speed) {
		Speed = speed;
	}

	public List<VehicleClass> getVehicleClasses() {
		if (this.vehicleClasses == null) {
			this.vehicleClasses = new ArrayList<VehicleClass>();
		}
		return vehicleClasses;
	}

	public void setVehicleClasses(List<VehicleClass> vehicleClasses) {
		this.vehicleClasses = vehicleClasses;
	}

	public Integer getSmallCount() {
		return smallCount;
	}

	public void setSmallCount(Integer smallCount) {
		this.smallCount = smallCount;
	}

	public Integer getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(Integer mediumCount) {
		this.mediumCount = mediumCount;
	}

	public Integer getLargeCount() {
		return largeCount;
	}

	public void setLargeCount(Integer largeCount) {
		this.largeCount = largeCount;
	}

	public void addSmallCount(int smallCount) {
		this.smallCount += smallCount;
	}

	public void addMediumCount(int mediumCount) {
		this.mediumCount += mediumCount;
	}

	public void addLargeCount(int largeCount) {
		this.largeCount += largeCount;
	}

	public void addCount(int count) {
		this.count += count;
	}

	public void addOccupancy(int occupancy) {
		this.occupancy += occupancy;
	}

	public void addVolume(int volume) {
		this.volume += volume;
	}

	public void updateWeightedSpeed(int newSpeed, int weight) {
		this.Speed = (weight * this.Speed + newSpeed) / (weight + 1);
	}

	public int getSpeedWeight() {
		return speedWeight;
	}

	public void setSpeedWeight(int speedWeight) {
		this.speedWeight = speedWeight;
	}

	public void addSpeedWeight(int speedWeight) {
		this.speedWeight += speedWeight;
	}

	@JsonIgnore
	public int getWeightedSpeed() {
		if (this.count != 0)
			return this.speedWeight / this.count;
		else
			return 0;
	}
	
	public void updateWeightedOccupancy(int newOccupancy, int weight) {
		this.occupancy = ((weight * this.occupancy) + newOccupancy)/(weight+1);
	}

}
