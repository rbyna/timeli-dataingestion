package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class CollectionPeriod {

	@JacksonXmlProperty(localName = "detection-time-stamp")
	protected LastUpdateTime detectionTimeStamp;
	@JacksonXmlProperty(localName = "start-time")
	protected String startTime;
	@JacksonXmlProperty(localName = "end-time")
	protected String endTime;
	@JacksonXmlElementWrapper(localName = "detector-reports")
	@JacksonXmlProperty(localName = "detector-report")
	protected List<DetectorReport> detectorReports;

	public LastUpdateTime getDetectionTimeStamp() {
		return detectionTimeStamp;
	}

	public void setDetectionTimeStamp(LastUpdateTime detectionTimeStamp) {
		this.detectionTimeStamp = detectionTimeStamp;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<DetectorReport> getDetectorReports() {
		if (this.detectorReports == null) {
			this.detectorReports = new ArrayList<DetectorReport>();
		}
		return detectorReports;
	}

	public void setDetectorReports(List<DetectorReport> detectorReports) {
		this.detectorReports = detectorReports;
	}

}
