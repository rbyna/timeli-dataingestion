package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
@JacksonXmlRootElement(localName = "detectorInventory")
public class DetectorInventory {

	@JacksonXmlProperty(localName = "organization-id")
	protected String organizationId;
	@JacksonXmlProperty(localName = "network-id")
	protected String networkId;
	@JacksonXmlElementWrapper(localName = "detector-list")
	@JacksonXmlProperty(localName = "detector")
	protected List<Detector> detectorList;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public List<Detector> getDetectorList() {
		if (this.detectorList == null) {
			this.detectorList = new ArrayList<Detector>();
		}
		return detectorList;
	}

	public void setDetectorList(List<Detector> detectorList) {
		this.detectorList = detectorList;
	}

}
