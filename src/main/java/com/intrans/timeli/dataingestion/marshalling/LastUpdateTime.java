package com.intrans.timeli.dataingestion.marshalling;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class LastUpdateTime {
	@JacksonXmlProperty(localName = "local-date")
	protected String localDate;
	@JacksonXmlProperty(localName = "local-time")
	protected String localTime;
	@JacksonXmlProperty(localName = "utc-offset")
	protected String utcOffset;

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}

	public String getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

}
